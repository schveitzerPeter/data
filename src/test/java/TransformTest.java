import org.example.Transform;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TransformTest {
    private final List<JSONObject> testDataJsonObjects = List.of(
        new JSONObject("{\"id\":\"11185349968\",\"type\":\"CreateEvent\",\"actor\":{\"id\":13025337,\"login\":\"bitnami-bot\",\"display_login\":\"bitnami-bot\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/bitnami-bot\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/13025337?\"},\"repo\":{\"id\":110563310,\"name\":\"bitnami/bitnami-docker-python\",\"url\":\"https://api.github.com/repos/bitnami/bitnami-docker-python\"},\"payload\":{\"ref\":\"3.8.1-debian-9-r10-prod\",\"ref_type\":\"tag\",\"master_branch\":\"master\",\"description\":\"Bitnami Python Docker Image http://bitnami.com/docker\",\"pusher_type\":\"user\"},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\",\"org\":{\"id\":5446553,\"login\":\"bitnami\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/orgs/bitnami\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/5446553?\"}}"),
        new JSONObject("{\"id\":\"11185349970\",\"type\":\"CreateEvent\",\"actor\":{\"id\":29139614,\"login\":\"renovate[bot]\",\"display_login\":\"renovate\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/renovate[bot]\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/29139614?\"},\"repo\":{\"id\":69679985,\"name\":\"amilajack/eslint-plugin-compat\",\"url\":\"https://api.github.com/repos/amilajack/eslint-plugin-compat\"},\"payload\":{\"ref\":\"renovate/semver-7.x\",\"ref_type\":\"branch\",\"master_branch\":\"master\",\"description\":\"Lint the browser compatibility of your code\",\"pusher_type\":\"user\"},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\"}"),
        new JSONObject("{\"id\":\"11185349982\",\"type\":\"PushEvent\",\"actor\":{\"id\":49699333,\"login\":\"dependabot[bot]\",\"display_login\":\"dependabot\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/dependabot[bot]\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/49699333?\"},\"repo\":{\"id\":206577258,\"name\":\"erfankashani/devcamp_portfolio\",\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio\"},\"payload\":{\"push_id\":4450652988,\"size\":3,\"distinct_size\":1,\"ref\":\"refs/heads/dependabot/bundler/puma-3.12.2\",\"head\":\"50272501e41fb95634019f9a225f5df206d37873\",\"before\":\"7d396aa9286e0e96bc6e5bccdf5a9efe44fc9909\",\"commits\":[{\"sha\":\"9a2c195648f2678a9721c413318652c7cd108a8a\",\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"message\":\"Bump rack from 2.0.7 to 2.0.8\\n\\nBumps [rack](https://github.com/rack/rack) from 2.0.7 to 2.0.8.\\n- [Release notes](https://github.com/rack/rack/releases)\\n- [Changelog](https://github.com/rack/rack/blob/master/CHANGELOG.md)\\n- [Commits](https://github.com/rack/rack/compare/2.0.7...2.0.8)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"distinct\":false,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/9a2c195648f2678a9721c413318652c7cd108a8a\"},{\"sha\":\"1f687093e69ce6a77728687f25c44c49066fd9ea\",\"author\":{\"name\":\"erfankashani\",\"email\":\"cb373ccf2f8891aa48fbe087662477dd0b13bd06@gmail.com\"},\"message\":\"Merge pull request #4 from erfankashani/dependabot/bundler/rack-2.0.8\\n\\nBump rack from 2.0.7 to 2.0.8\",\"distinct\":false,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/1f687093e69ce6a77728687f25c44c49066fd9ea\"},{\"sha\":\"50272501e41fb95634019f9a225f5df206d37873\",\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"message\":\"Bump puma from 3.12.1 to 3.12.2\\n\\nBumps [puma](https://github.com/puma/puma) from 3.12.1 to 3.12.2.\\n- [Release notes](https://github.com/puma/puma/releases)\\n- [Changelog](https://github.com/puma/puma/blob/master/History.md)\\n- [Commits](https://github.com/puma/puma/compare/v3.12.1...v3.12.2)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"distinct\":true,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/50272501e41fb95634019f9a225f5df206d37873\"}]},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\"}"),
        new JSONObject("{\"id\":\"11185349988\",\"type\":\"PushEvent\",\"actor\":{\"id\":2299951,\"login\":\"pietroalbini\",\"display_login\":\"pietroalbini\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/pietroalbini\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/2299951?\"},\"repo\":{\"id\":26193547,\"name\":\"rust-lang/crates.io-index\",\"url\":\"https://api.github.com/repos/rust-lang/crates.io-index\"},\"payload\":{\"push_id\":4450652992,\"size\":1,\"distinct_size\":1,\"ref\":\"refs/heads/master\",\"head\":\"7da92379e91d708323569780ce32a6aab29b0b76\",\"before\":\"3a066faf940a423d1d47885a1de133e1313cb61c\",\"commits\":[{\"sha\":\"7da92379e91d708323569780ce32a6aab29b0b76\",\"author\":{\"name\":\"bors\",\"email\":\"3a2c29237f73c7cf88424b80e427b1884f346b35@rust-lang.org\"},\"message\":\"Updating crate `protobuf#2.10.0`\",\"distinct\":true,\"url\":\"https://api.github.com/repos/rust-lang/crates.io-index/commits/7da92379e91d708323569780ce32a6aab29b0b76\"}]},\"public\":true,\"created_at\":\"2020-01-01T00:41:02Z\",\"org\":{\"id\":5430905,\"login\":\"rust-lang\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/orgs/rust-lang\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/5430905?\"}}")
    );

    private final List<JSONObject> createEvents = List.of(
            new JSONObject("{\"id\":\"11185349968\",\"type\":\"CreateEvent\",\"actor\":{\"id\":13025337,\"login\":\"bitnami-bot\",\"display_login\":\"bitnami-bot\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/bitnami-bot\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/13025337?\"},\"repo\":{\"id\":110563310,\"name\":\"bitnami/bitnami-docker-python\",\"url\":\"https://api.github.com/repos/bitnami/bitnami-docker-python\"},\"payload\":{\"ref\":\"3.8.1-debian-9-r10-prod\",\"ref_type\":\"tag\",\"master_branch\":\"master\",\"description\":\"Bitnami Python Docker Image http://bitnami.com/docker\",\"pusher_type\":\"user\"},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\",\"org\":{\"id\":5446553,\"login\":\"bitnami\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/orgs/bitnami\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/5446553?\"}}"),
            new JSONObject("{\"id\":\"11185349970\",\"type\":\"CreateEvent\",\"actor\":{\"id\":29139614,\"login\":\"renovate[bot]\",\"display_login\":\"renovate\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/renovate[bot]\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/29139614?\"},\"repo\":{\"id\":69679985,\"name\":\"amilajack/eslint-plugin-compat\",\"url\":\"https://api.github.com/repos/amilajack/eslint-plugin-compat\"},\"payload\":{\"ref\":\"renovate/semver-7.x\",\"ref_type\":\"branch\",\"master_branch\":\"master\",\"description\":\"Lint the browser compatibility of your code\",\"pusher_type\":\"user\"},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\"}")
    );

    private final List<JSONObject> testDataJsonObjectsWithOnlyPushEvents = List.of(
        new JSONObject("{\"id\":\"11185349982\",\"type\":\"PushEvent\",\"actor\":{\"id\":49699333,\"login\":\"dependabot[bot]\",\"display_login\":\"dependabot\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/dependabot[bot]\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/49699333?\"},\"repo\":{\"id\":206577258,\"name\":\"erfankashani/devcamp_portfolio\",\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio\"},\"payload\":{\"push_id\":4450652988,\"size\":3,\"distinct_size\":1,\"ref\":\"refs/heads/dependabot/bundler/puma-3.12.2\",\"head\":\"50272501e41fb95634019f9a225f5df206d37873\",\"before\":\"7d396aa9286e0e96bc6e5bccdf5a9efe44fc9909\",\"commits\":[{\"sha\":\"9a2c195648f2678a9721c413318652c7cd108a8a\",\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"message\":\"Bump rack from 2.0.7 to 2.0.8\\n\\nBumps [rack](https://github.com/rack/rack) from 2.0.7 to 2.0.8.\\n- [Release notes](https://github.com/rack/rack/releases)\\n- [Changelog](https://github.com/rack/rack/blob/master/CHANGELOG.md)\\n- [Commits](https://github.com/rack/rack/compare/2.0.7...2.0.8)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"distinct\":false,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/9a2c195648f2678a9721c413318652c7cd108a8a\"},{\"sha\":\"1f687093e69ce6a77728687f25c44c49066fd9ea\",\"author\":{\"name\":\"erfankashani\",\"email\":\"cb373ccf2f8891aa48fbe087662477dd0b13bd06@gmail.com\"},\"message\":\"Merge pull request #4 from erfankashani/dependabot/bundler/rack-2.0.8\\n\\nBump rack from 2.0.7 to 2.0.8\",\"distinct\":false,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/1f687093e69ce6a77728687f25c44c49066fd9ea\"},{\"sha\":\"50272501e41fb95634019f9a225f5df206d37873\",\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"message\":\"Bump puma from 3.12.1 to 3.12.2\\n\\nBumps [puma](https://github.com/puma/puma) from 3.12.1 to 3.12.2.\\n- [Release notes](https://github.com/puma/puma/releases)\\n- [Changelog](https://github.com/puma/puma/blob/master/History.md)\\n- [Commits](https://github.com/puma/puma/compare/v3.12.1...v3.12.2)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"distinct\":true,\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/50272501e41fb95634019f9a225f5df206d37873\"}]},\"public\":true,\"created_at\":\"2020-01-01T00:41:01Z\"}"),
        new JSONObject("{\"id\":\"11185349988\",\"type\":\"PushEvent\",\"actor\":{\"id\":2299951,\"login\":\"pietroalbini\",\"display_login\":\"pietroalbini\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/pietroalbini\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/2299951?\"},\"repo\":{\"id\":26193547,\"name\":\"rust-lang/crates.io-index\",\"url\":\"https://api.github.com/repos/rust-lang/crates.io-index\"},\"payload\":{\"push_id\":4450652992,\"size\":1,\"distinct_size\":1,\"ref\":\"refs/heads/master\",\"head\":\"7da92379e91d708323569780ce32a6aab29b0b76\",\"before\":\"3a066faf940a423d1d47885a1de133e1313cb61c\",\"commits\":[{\"sha\":\"7da92379e91d708323569780ce32a6aab29b0b76\",\"author\":{\"name\":\"bors\",\"email\":\"3a2c29237f73c7cf88424b80e427b1884f346b35@rust-lang.org\"},\"message\":\"Updating crate `protobuf#2.10.0`\",\"distinct\":true,\"url\":\"https://api.github.com/repos/rust-lang/crates.io-index/commits/7da92379e91d708323569780ce32a6aab29b0b76\"}]},\"public\":true,\"created_at\":\"2020-01-01T00:41:02Z\",\"org\":{\"id\":5430905,\"login\":\"rust-lang\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/orgs/rust-lang\",\"avatar_url\":\"https://avatars.githubusercontent.com/u/5430905?\"}}")
    );
    private final List<JSONObject> testDataCommits = List.of(
        new JSONObject("{\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"distinct\":false,\"message\":\"Bump rack from 2.0.7 to 2.0.8\\n\\nBumps [rack](https://github.com/rack/rack) from 2.0.7 to 2.0.8.\\n- [Release notes](https://github.com/rack/rack/releases)\\n- [Changelog](https://github.com/rack/rack/blob/master/CHANGELOG.md)\\n- [Commits](https://github.com/rack/rack/compare/2.0.7...2.0.8)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"sha\":\"9a2c195648f2678a9721c413318652c7cd108a8a\",\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/9a2c195648f2678a9721c413318652c7cd108a8a\"}"),
        new JSONObject("{\"author\":{\"name\":\"erfankashani\",\"email\":\"cb373ccf2f8891aa48fbe087662477dd0b13bd06@gmail.com\"},\"distinct\":false,\"message\":\"Merge pull request #4 from erfankashani/dependabot/bundler/rack-2.0.8\\n\\nBump rack from 2.0.7 to 2.0.8\",\"sha\":\"1f687093e69ce6a77728687f25c44c49066fd9ea\",\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/1f687093e69ce6a77728687f25c44c49066fd9ea\"}"),
        new JSONObject("{\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"distinct\":true,\"message\":\"Bump puma from 3.12.1 to 3.12.2\\n\\nBumps [puma](https://github.com/puma/puma) from 3.12.1 to 3.12.2.\\n- [Release notes](https://github.com/puma/puma/releases)\\n- [Changelog](https://github.com/puma/puma/blob/master/History.md)\\n- [Commits](https://github.com/puma/puma/compare/v3.12.1...v3.12.2)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"sha\":\"50272501e41fb95634019f9a225f5df206d37873\",\"url\":\"https://api.github.com/repos/erfankashani/devcamp_portfolio/commits/50272501e41fb95634019f9a225f5df206d37873\"}"),
        new JSONObject("{\"author\":{\"name\":\"dependabot[bot]\",\"email\":\"1c358da00a777d4e9898c1280ab801e2df165188@users.noreply.github.com\"},\"distinct\":false,\"message\":\"Bump handlebars from 4.1.2 to 4.5.3\\n\\nBumps [handlebars](https://github.com/wycats/handlebars.js) from 4.1.2 to 4.5.3.\\n- [Release notes](https://github.com/wycats/handlebars.js/releases)\\n- [Changelog](https://github.com/wycats/handlebars.js/blob/master/release-notes.md)\\n- [Commits](https://github.com/wycats/handlebars.js/compare/v4.1.2...v4.5.3)\\n\\nSigned-off-by: dependabot[bot] <support@github.com>\",\"sha\":\"08e36339a51e4473a71adb7dac0f82132b751f91\",\"url\":\"https://api.github.com/repos/kevtan/prototype-react/commits/08e36339a51e4473a71adb7dac0f82132b751f91\"}")
    );
    private final Transform transform = new Transform();
    @Test
    void filterPushEvents_listOfVariableTypes_returnsOnlyPushEvents() {
        List<JSONObject> result = transform.filterPushEvents(testDataJsonObjects);
        for (JSONObject object : result) {
            assertEquals("PushEvent", object.getString("type"));
        }
        assertEquals(2, result.size());
    }

    @Test
    void filterPushEvents_listOfNotEventTypes_returnsEmptyList() {
        List<JSONObject> result = transform.filterPushEvents(createEvents);
        assertEquals(0, result.size());
    }

    @Test
    void filterPushEvents_emptyList_returnsEmptyList() {
        List<JSONObject> result = transform.filterPushEvents(List.of());
         assertEquals(0, result.size());
    }

    @Test
    void getCommits_listOfEventsWithFourCommitsInTotal_returnsCommitList(){
        List<JSONObject> result = transform.getCommits(testDataJsonObjectsWithOnlyPushEvents);
        assertEquals(4,result.size());
    }

    @Test
    void getCommits_listThatDoesNotContainCommits_returnsEmptyList(){
        List<JSONObject> result = transform.getCommits(testDataJsonObjectsWithOnlyPushEvents);
        assertNotEquals(0,result.size());
    }

    @Test
    void getCommits_emptyList_returnsEmptyList(){
        List<JSONObject> result = transform.getCommits(List.of());
        assertEquals(0,result.size());
    }

    @Test
    void concatMessages_emptyMap_returnsEmptyMap(){
        Map<String, StringBuilder> input = Map.of();
        var result = transform.concatMessages(input);
        assertEquals(0,result.size());
    }

    @Test
    void concatMessages_mapOfCommitsByAuthor_returnsMapOfAuthorsAndValueAsString() {
        Map<String, StringBuilder> input = Map.of(
            "author", new StringBuilder("the quick brown fox jumps over the lazy dog the quick brown fox"),
            "author2", new StringBuilder("the quick")
        );

        Transform mock = mock(Transform.class);
        when(mock.findTopTrigrams(String.valueOf(input.get("author")))).thenReturn(List.of(
            "the quick brown",
            "quick brown fox",
            "brown fox jumps",
            "fox jumps over",
            "jumps over the"
        ));

        var result = transform.concatMessages(input);
        assertEquals(2, result.size());
        assertEquals(5, result.get("author").size());
        assertEquals(0, result.get("author2").size());
    }

    @Test
    public void findTopTrigrams_inputOfSevenTrigrams_returnsListOfTopFiveTrigrams() {
        String input = "the quick brown fox jumps over the lazy dog the quick brown fox";
        Map<String, Integer> trigramsMap = Map.of(
                "the quick brown", 2,
                "quick brown fox", 1,
                "brown fox jumps", 1,
                "fox jumps over", 1,
                "jumps over the", 1,
                "over the lazy", 1,
                "the lazy dog", 1
        );

        Transform mock = mock(Transform.class);

        when(mock.countTrigramOccurrences(input)).thenReturn(trigramsMap);

        List<String> result = transform.findTopTrigrams(input);

        assertEquals(5, result.size());
        assertEquals("the quick brown",result.get(0));
        assertEquals("quick brown fox",result.get(1));
        assertEquals("jumps over the",result.get(4));
    }

    @Test
    void aggregateCommitMessages_listOfCommits_returnsAggregatedCommitsByAuthor() {
        var aggregatedCommitsByAuthor = transform.aggregateCommitMessages(testDataCommits);
        var concatenatedCommitMessagesOfUserDependabot = aggregatedCommitsByAuthor.get("dependabot[bot]").toString();
        assertTrue(concatenatedCommitMessagesOfUserDependabot.contains("wycats"));
    }

    @Test
    void aggregateCommitMessages_emptyList_returnsEmptyList() {
        var aggregatedCommitsByAuthor = transform.aggregateCommitMessages(List.of());
        assertEquals(0, aggregatedCommitsByAuthor.size());
    }

    @Test
    void countTrigramOccurrences_emptyList_returnsEmptyList() {
        Map<String, Integer> result = transform.countTrigramOccurrences("");
        assertEquals(0, result.size());
    }

    @Test
    void countTrigramOccurrences_noTrigrams_returnsEmptyList() {
        Map<String, Integer> result = transform.countTrigramOccurrences("apple orange");
        assertEquals(0, result.size());
    }

    @Test
    void countTrigramOccurrences_twoTrigrams_returnsListOfTrigrams() {
        Map<String, Integer> result = transform.countTrigramOccurrences("apple banana cherry mango");
        assertEquals(2, result.size());
        assertEquals(1, result.get("apple banana cherry"));
        assertEquals(1, result.get("banana cherry mango"));
    }

    @Test
    void countTrigramOccurrences_multipleOccurrenceOfTrigram_returnsListOfTrigrams() {
        Map<String, Integer> result = transform.countTrigramOccurrences("apple banana cherry apple banana cherry");
        assertEquals(3, result.size());
        assertEquals(2, result.get("apple banana cherry"));
    }
}