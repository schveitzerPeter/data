package org.example;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.*;
import java.util.stream.IntStream;
public class Transform {

    /**
     * Filters a list of JSONObjects to include only those with the type "PushEvent".
     *
     * @param jsonObjects The list of JSONObjects to filter.
     * @return A list containing only the JSONObjects with the type "PushEvent".
     */
    public List<JSONObject> filterPushEvents(List<JSONObject> jsonObjects){
        return jsonObjects.stream()
            .filter(jsonObject -> jsonObject.getString("type").equals("PushEvent"))
            .toList();
    }

    /**
     * Extracts the commit JSONObjects from a list of JSONObjects.
     *
     * @param jsonObjects The list of JSONObjects containing payload and commit array.
     * @return A list containing all the commit JSONObjects extracted from the commit array.
     */
    public List<JSONObject> getCommits(List<JSONObject> jsonObjects) {
        return jsonObjects.stream()
            .map(jsonObject -> {
                JSONArray commitsArray = jsonObject.getJSONObject("payload").getJSONArray("commits");
                return IntStream.range(0, commitsArray.length())
                        .mapToObj(commitsArray::getJSONObject)
                        .toList();
            })
            .flatMap(List::stream)
            .toList();
    }

    /**
     * Aggregates commit messages for each author.
     *
     * @param jsonObjects The list of JSONObjects representing commits.
     * @return A map containing author names as keys and a list of their commit messages as values.
     */
    public Map<String, StringBuilder> aggregateCommitMessages(List<JSONObject> jsonObjects) {
        Map<String, StringBuilder> authorMessagesMap = new HashMap<>();

        for (JSONObject jsonObject : jsonObjects) {
            JSONObject author = jsonObject.getJSONObject("author");
            String authorName = author.getString("name");
            String message = jsonObject.getString("message");
            message = message.toLowerCase().replaceAll("[.,'\"?!\\[\\]{}()\\-—:;…]", "");

            if(authorMessagesMap.containsKey(authorName)){
                authorMessagesMap.get(authorName).append(message).append(" ");
            } else {
                authorMessagesMap.put(authorName, new StringBuilder(message+" "));
            }
        }
        return authorMessagesMap;
    }

    /**
     * Concatenates the messages for each author in the authorMessagesMap and finds the top trigrams for each author.
     *
     * @param authorMessagesMap A map containing author names as keys and StringBuilder objects containing their messages as values.
     * @return A map containing author names as keys and a list of their top trigrams as values.
     */
    public Map<String, List<String>> concatMessages( Map<String, StringBuilder>authorMessagesMap ){
        Map<String, List<String>> result = new HashMap<>();
        authorMessagesMap.forEach((author, messageBuilder) -> {
            String concatMessages = messageBuilder.toString();
            result.put(
                author,
                findTopTrigrams(concatMessages)
            );
        });
        return result;
    }

    /**
     * Finds the top 5 trigrams in the given text based on their occurrences.
     *
     * @param text The text in which to find the top trigrams.
     * @return A list containing the top 5 trigrams based on their occurrences in the text.
     */
    public List<String> findTopTrigrams(String text) {
        // Count occurrences of trigrams in the text
        var trigramsMap = countTrigramOccurrences(text);

        // Sort trigrams by their occurrences in descending order
        List<Map.Entry<String, Integer>> sortedTrigrams = trigramsMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .toList();

        // Extract the top 5 trigrams
        List<String> top5Trigrams = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : sortedTrigrams) {
            if (top5Trigrams.size() >= 5) {
                break;
            }
            top5Trigrams.add(entry.getKey());
        }
        return top5Trigrams;
    }

    /**
     * Counts the occurrences of trigrams in the given text.
     *
     * @param text The text in which to count trigram occurrences.
     * @return A map containing trigrams as keys and their occurrences as values.
     */
    public Map<String, Integer> countTrigramOccurrences(String text){
        Map<String, Integer> count = new LinkedHashMap<>();
        String[] words = text.split("\\s+");
        for (int i = 0; i < words.length - 2; i++) {
            String trigram = words[i] + " " + words[i + 1] + " " + words[i + 2];
            if(count.containsKey(trigram)){
                Integer occurrence = count.get(trigram);
                count.put(trigram,occurrence+1);
            } else {
                count.put(trigram,1);
            }
        }
        return count;
    }
}