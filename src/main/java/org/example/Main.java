package org.example;

public class Main {
    public static void main(String[] args) {
        var transformer = new Transform();
        //importing all json-objects
        var importer = new JsonImporter("10K.github.jsonl");

        //filtering out object by type event
        var filteredPushEvents = transformer.filterPushEvents(importer.getJsonObjects());

        //collecting all commit objects into one list
        var commits = transformer.getCommits(filteredPushEvents);

        //making a map from commitList by author as a key and value as a stringbuilder which holds all commit msg
        var aggregateCommitMessages = transformer.aggregateCommitMessages(commits);

        //making a map from the aggregated messages where key is the author just like before but the value is
        //string now forged from the string builder values
        var concatenatedCommitMessages = transformer.concatMessages(aggregateCommitMessages);

        //saving the trigrams into a csv file
        var exporter = new CSVExporter(concatenatedCommitMessages, "result.csv");
    }
}