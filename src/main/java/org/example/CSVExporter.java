package org.example;

import com.opencsv.CSVWriter;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * A class responsible for exporting data to a CSV file.
 * It takes a map of authors and their corresponding trigrams as input and writes them to a CSV file.
 */
public class CSVExporter {

    public CSVExporter(Map<String,List<String>> data, String fileName) {
        try (CSVWriter writer = new CSVWriter(new FileWriter(fileName))) {

            writer.writeNext(new String[]{"author", "first 3-gram", "second 3-gram", "third 3-gram", "fourth 3-gram", "fifth 3-gram"});

            for (Map.Entry<String, List<String>> entry : data.entrySet()) {
                String author = entry.getKey();
                List<String> trigrams = entry.getValue();
                String[] rowData = new String[6];
                rowData[0] = author;

                for (int i = 0; i < 5; i++) {
                    rowData[i + 1] = (i < trigrams.size()) ? trigrams.get(i) : "";
                }

                writer.writeNext(rowData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
