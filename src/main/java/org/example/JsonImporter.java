package org.example;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class responsible for importing JSON data from a file.
 * It reads the JSON objects from the file and stores them in a list of JSONObjects.
 */
public class JsonImporter {
    private final List<JSONObject> jsonObjects = new ArrayList<>();
    public JsonImporter(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                JSONObject jsonObject = new JSONObject(line);
                jsonObjects.add(jsonObject);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public List<JSONObject> getJsonObjects() {
        return jsonObjects;
    }
}
